package com.code.transport.info.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "line")
@DynamicUpdate
public class LineEntity{
    @Id
    @Column(name = "line_number")
    private int lineNumber;
    private String lineDesignation;
    private String transportMode;
    private String transportModeCode;
    private String lastModifiedUtcDateTime;
    private String existsFromDate;
    private int stopsCount;
    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn(name = "line_number")
    private LineRouteEntity lineRoute;

}
