package com.code.transport.info.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "journey")
public class JourneyEntity{

    @Id
    private int journeyPatternPointNumber;
    private int lineNumber;
    private String directionCode;
    private String lastModifiedUtcDateTime;
    private String existsFromDate;

}
