package com.code.transport.info.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "stop")
public class StopEntity{
    @Id
    private int stopPointNumber;
    private String stopPointName;
    private int stopAreaNumber;
    private String locationNorthingCoordinate;
    private String locationEastingCoordinate;
    private String zoneShortName;
    private String existsFromDate;
    private String stopAreaTypeCode;
    private String lastModifiedUtcDateTime;

}

