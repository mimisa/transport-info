package com.code.transport.info.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "line_route")
public class LineRouteEntity {
    @Id
    @Column(name = "line_number")
    private int lineNumber;
    @Column(name = "stop_count")
    private int stopCount;
    @ManyToMany(cascade = CascadeType.ALL)
    private Set<StopEntity> stops;

}
