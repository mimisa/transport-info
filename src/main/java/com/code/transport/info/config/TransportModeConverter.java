package com.code.transport.info.config;


import com.code.transport.info.model.TransportMode;
import org.springframework.core.convert.converter.Converter;

public class TransportModeConverter implements Converter<String, TransportMode> {
    @Override
    public TransportMode convert(String value) {
       return TransportMode.valueOf(value);
    }
}
