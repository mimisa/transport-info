package com.code.transport.info.config;

import com.code.transport.info.batch.LineRouteTasklet;
import com.code.transport.info.batch.processor.TrafficJourneyProcessor;
import com.code.transport.info.batch.processor.TrafficLineProcessor;
import com.code.transport.info.batch.processor.TrafficStopProcessor;
import com.code.transport.info.batch.reader.TrafficInfoReader;
import com.code.transport.info.batch.writer.JpaTrafficInfoWriter;
import com.code.transport.info.entity.JourneyEntity;
import com.code.transport.info.entity.LineEntity;
import com.code.transport.info.entity.StopEntity;
import com.code.transport.info.model.TrafficJourney;
import com.code.transport.info.model.TrafficLine;
import com.code.transport.info.model.TrafficStop;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@EnableBatchProcessing
@EnableScheduling
@Configuration
public class BatchConfig {
    private static final String JOB_TRAFFIC_INFO = "JOB_TRAFFIC_INFO";
    private static final String JOB_TRAFFIC_ROUTE_BUILDER = "JOB_TRAFFIC_ROUTE_BUILDER";

    private static final String STEP_LINE_INFO = "STEP_LINE_INFO";
    private static final String STEP_STOP_INFO = "STEP_STOP_INFO";
    private static final String STEP_JOURNEY_INFO = "STEP_JOURNEY_INFO";
    private static final String STEP_LINE_ROUTE = "STEP_LINE_ROUTE";

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    Environment environment;

    @Bean
    @Order(1)
    @Qualifier(JOB_TRAFFIC_INFO)
    public Job jobTrafficInfo() {
        return jobBuilderFactory.get(JOB_TRAFFIC_INFO)
                .start(lineInfoStep())
                .next(journeyInfoStep())
                .next(stopInfoStep())
                .build();
    }

    @Bean
    @Order(2)
    @Qualifier(JOB_TRAFFIC_ROUTE_BUILDER)
    public Job job2() {
        return jobBuilderFactory.get(JOB_TRAFFIC_ROUTE_BUILDER)
                .start(lineRouteStep())
                .build();
    }

    //** Steps
    @Bean
    public Step lineInfoStep() {
        return stepBuilderFactory.get(STEP_LINE_INFO)
                .<TrafficLine, LineEntity>chunk(20)
                .reader(lineReader())
                .processor(lineProcessor)
                .writer(lineWriter)
                .build();
    }

    @Bean
    public Step journeyInfoStep() {
        return stepBuilderFactory.get(STEP_JOURNEY_INFO)
                .<TrafficJourney, JourneyEntity>chunk(10)
                .reader(journeyReader())
                .processor(journeyProcessor)
                .writer(journeyWriter)
                .build();
    }

    @Bean
    public Step stopInfoStep() {
        return stepBuilderFactory.get(STEP_STOP_INFO)
                .<TrafficStop, StopEntity>chunk(10)
                .reader(stopReader())
                .processor(stopProcessor)
                .writer(stopWriter)
                .build();
    }

    @Bean
    protected Step lineRouteStep() {
        return stepBuilderFactory.get(STEP_LINE_ROUTE)
                .tasklet(lineRouteTasklet)
                .build();
    }

    //** Readers
    @Bean
    ItemReader<TrafficLine> lineReader() {
        return TrafficInfoReader.<TrafficLine>builder()
                .restTemplate(restTemplate)
                .objectMapper(objectMapper)
                .apiKey(environment.getProperty("api.key"))
                .apiUrl(environment.getProperty("api.url"))
                .model("line")
                .type(TrafficLine.class)
                .build();
    }

    @Bean
    ItemReader<TrafficJourney> journeyReader() {
        return TrafficInfoReader.<TrafficJourney>builder()
                .restTemplate(restTemplate)
                .objectMapper(objectMapper)
                .apiKey(environment.getProperty("api.key"))
                .apiUrl(environment.getProperty("api.url"))
                .model("jour")
                .type(TrafficJourney.class)
                .build();
    }

    @Bean
    ItemReader<TrafficStop> stopReader() {
        return TrafficInfoReader.<TrafficStop>builder()
                .restTemplate(restTemplate)
                .objectMapper(objectMapper)
                .apiKey(environment.getProperty("api.key"))
                .apiUrl(environment.getProperty("api.url"))
                .model("stop")
                .type(TrafficStop.class)
                .build();
    }

    //** Item processors
    @Autowired
    TrafficLineProcessor lineProcessor;
    @Autowired
    TrafficStopProcessor stopProcessor;
    @Autowired
    TrafficJourneyProcessor journeyProcessor;
    @Autowired
    LineRouteTasklet lineRouteTasklet;

    //** Item writers
    @Autowired
    JpaTrafficInfoWriter<LineEntity> lineWriter;
    @Autowired
    JpaTrafficInfoWriter<StopEntity> stopWriter;
    @Autowired
    JpaTrafficInfoWriter<JourneyEntity> journeyWriter;


}
