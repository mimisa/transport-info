package com.code.transport.info.service;

import com.code.transport.info.dto.TransportLineDto;
import com.code.transport.info.entity.JourneyEntity;
import com.code.transport.info.entity.LineEntity;
import com.code.transport.info.entity.LineRouteEntity;
import com.code.transport.info.exception.TraffiInfoNotFoundException;
import com.code.transport.info.model.TransportMode;
import com.code.transport.info.repository.JourneyRepository;
import com.code.transport.info.repository.LineRepository;
import com.code.transport.info.repository.StopRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Component
@CacheConfig(cacheNames = {"traffic-info"})
@Slf4j

public class TransportService {

    private final JourneyRepository journeyRepository;
    private final StopRepository stopRepository;
    private final LineRepository lineRepository;
    private final ModelMapper modelMapper;

    public void updateLinesWithRoutes() {

        Map<Integer, List<JourneyEntity>> journeyMap = journeyRepository.findAll().stream()
                .collect(Collectors.groupingBy(JourneyEntity::getLineNumber));

        Map<Integer, LineRouteEntity> routes = journeyMap.entrySet()
                .stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> createLineRouteEntity(entry)
                ));
        updateLines(routes);
    }

    private LineRouteEntity createLineRouteEntity(Map.Entry<Integer, List<JourneyEntity>> entry) {
        Set<Integer> stopIds = entry.getValue().stream()
                .map(journey -> journey.getJourneyPatternPointNumber())
                .collect(Collectors.toSet());

        return LineRouteEntity.builder()
                .lineNumber(entry.getKey())
                .stopCount(entry.getValue().size())
                .stops(stopRepository.findByStopAreaNumberIn(stopIds))
                .build();
    }

    private Stream<LineEntity> lineToUpdate(Map<Integer, LineRouteEntity> routes) {
        return lineRepository.findByLineNumberIn(routes.keySet());
    }

    @Transactional
    void updateLines(Map<Integer, LineRouteEntity> routes) {
        List<LineEntity> lines = lineToUpdate(routes)
                .map(lineEntity -> {
                    int key = lineEntity.getLineNumber();
                    lineEntity.setLineRoute(routes.get(key));
                    lineEntity.setStopsCount(routes.get(key).getStopCount());
                    return lineEntity;
                })
                .collect(Collectors.toList());
        lineRepository.saveAll(lines);
    }

    @Cacheable
    public List<TransportLineDto> linesByTransportMode(TransportMode transportMode) {
        log.debug("Saving to cache lines by transport mode for {}.", transportMode);
        List<LineEntity> result = lineRepository.findTop10ByTransportModeCodeOrderByStopsCountDesc(transportMode.toString());
        return result.stream()
                .map(lineEntity -> dtoFrom(lineEntity, TransportLineDto.class))
                .collect(Collectors.toList());

    }
    @Cacheable
    public TransportLineDto lineInfo(TransportMode transportMode, int lineNumber) {
        log.debug("Saving to cache line info for {}.", lineNumber);
        LineEntity result = lineRepository.findFirstByTransportModeCodeAndLineNumber(transportMode.toString(), lineNumber);
        if(result == null ) throw new TraffiInfoNotFoundException(String.format("Line: {} for transport mode: {} not found", lineNumber, transportMode.toString()));
        return dtoFrom(result, TransportLineDto.class);
    }

    private <T, S> T dtoFrom(S source, Class<T> toType) {
        T dto = modelMapper.map(source, toType);
        return dto;
    }
}
