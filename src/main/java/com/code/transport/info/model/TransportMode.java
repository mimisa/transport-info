package com.code.transport.info.model;

public enum TransportMode {
    BUS,
    METRO,
    TRAM,
    TRAIN,
    SHIP,
    FERRY
}
