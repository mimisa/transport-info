package com.code.transport.info.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;

@Builder(builderClassName = "Builder")
@Getter
@JsonDeserialize(builder = TrafficStop.Builder.class)
public class TrafficStop {

        @JsonProperty("StopPointNumber")
        private final String stopPointNumber;
        @JsonProperty("StopPointName")
        private final String stopPointName;
        @JsonProperty("StopAreaNumber")
        private final String stopAreaNumber;
        @JsonProperty("LocationNorthingCoordinate")
        private final String locationNorthingCoordinate;
        @JsonProperty("LocationEastingCoordinate")
        private final String locationEastingCoordinate;
        @JsonProperty("ZoneShortName")
        private final String zoneShortName;
        @JsonProperty("ExistsFromDate")
        private final String existsFromDate;
        @JsonProperty("StopAreaTypeCode")
        private final String stopAreaTypeCode;
        @JsonProperty("LastModifiedUtcDateTime")
        private final String lastModifiedUtcDateTime;

        @JsonPOJOBuilder(withPrefix = "")
        public static class Builder {
        }
 }
