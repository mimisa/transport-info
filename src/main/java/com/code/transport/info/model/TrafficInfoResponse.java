package com.code.transport.info.model;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class TrafficInfoResponse<T> {
    private final List<T> responseData;
}
