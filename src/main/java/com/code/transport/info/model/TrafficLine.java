package com.code.transport.info.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.Value;

@Value
@Getter
@JsonDeserialize(builder = TrafficLine.Builder.class)
@Builder(builderClassName = "Builder")

public class TrafficLine {
    @JsonProperty("LineNumber")
    private final String lineNumber;
    @JsonProperty("LineDesignation")
    private final String lineDesignation;
    @JsonProperty("DefaultTransportMode")
    private final String defaultTransportMode;
    @JsonProperty("DefaultTransportModeCode")
    private final String defaultTransportModeCode;
    @JsonProperty("LastModifiedUtcDateTime")
    private final String lastModifiedUtcDateTime;
    @JsonProperty("ExistsFromDate")
    private final String existsFromDate;

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder {
    }

}
