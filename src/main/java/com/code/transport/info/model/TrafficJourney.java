package com.code.transport.info.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.Value;


@Builder(builderClassName = "Builder")
@Getter
@Value
@JsonDeserialize(builder = TrafficJourney.Builder.class)
public class TrafficJourney {

    @JsonProperty("LineNumber")
    private final String lineNumber;
    @JsonProperty("DirectionCode")
    private final String directionCode;
    @JsonProperty("JourneyPatternPointNumber")
    private final String journeyPatternPointNumber;
    @JsonProperty("LastModifiedUtcDateTime")
    private final String lastModifiedUtcDateTime;
    @JsonProperty("ExistsFromDate")
    private final String existsFromDate;

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder {
    }

}
