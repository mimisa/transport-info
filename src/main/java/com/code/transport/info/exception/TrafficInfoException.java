package com.code.transport.info.exception;

public class TrafficInfoException extends RuntimeException {

    public TrafficInfoException(String message, Throwable cause) {
        super(message, cause);
    }

    public TrafficInfoException(String message) {
        super(message);
    }

}
