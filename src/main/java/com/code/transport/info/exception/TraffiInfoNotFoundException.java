package com.code.transport.info.exception;

public class TraffiInfoNotFoundException extends RuntimeException {
    public TraffiInfoNotFoundException(String message) {
        super(message);
    }
}
