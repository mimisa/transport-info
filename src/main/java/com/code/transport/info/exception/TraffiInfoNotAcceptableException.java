package com.code.transport.info.exception;

public class TraffiInfoNotAcceptableException extends RuntimeException {

    public TraffiInfoNotAcceptableException(String message, Throwable cause) {
        super(message, cause);
    }

    public TraffiInfoNotAcceptableException(String message) {
        super(message);
    }

}
