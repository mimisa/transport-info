package com.code.transport.info.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransportLineDto {

    private int lineNumber;
    private String lineDesignation;
    private String transportMode;
    private String transportModeCode;
    private String lastModifiedUtcDateTime;
    private String existsFromDate;
    private int stopsCount;
    private TransportLineRouteDto lineRoute;

}
