package com.code.transport.info.dto;

import com.code.transport.info.entity.StopEntity;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransportLineRouteDto {

    private int lineNumber;
    private int stopCount;
    private Set<StopEntity> stops;

}
