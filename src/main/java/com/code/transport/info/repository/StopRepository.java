package com.code.transport.info.repository;

import com.code.transport.info.entity.StopEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface StopRepository extends JpaRepository<StopEntity, Integer> {
    Set<StopEntity> findByStopAreaNumberIn(Set<Integer> stopPointNumbers);
}
