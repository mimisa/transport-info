package com.code.transport.info.repository;

import com.code.transport.info.entity.LineRouteEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LineRouteRepository extends JpaRepository<LineRouteEntity, Integer> {
}
