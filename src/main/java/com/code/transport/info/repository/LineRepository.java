package com.code.transport.info.repository;

import com.code.transport.info.entity.LineEntity;
import com.code.transport.info.model.TransportMode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

public interface LineRepository extends JpaRepository<LineEntity, Integer> {
    Stream<LineEntity> findByLineNumberIn(Set<Integer> lineNumbers);
    List<LineEntity> findTop10ByTransportModeCodeOrderByStopsCountDesc(String transportMode);
    LineEntity findFirstByTransportModeCodeAndLineNumber(String transportMode, int lineNumber);
}
