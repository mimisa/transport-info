package com.code.transport.info.repository;

import com.code.transport.info.entity.JourneyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JourneyRepository extends JpaRepository<JourneyEntity, Integer> {
}
