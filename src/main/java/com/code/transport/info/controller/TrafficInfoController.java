package com.code.transport.info.controller;

import com.code.transport.info.dto.TransportLineDto;
import com.code.transport.info.model.TransportMode;
import com.code.transport.info.service.TransportService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@Slf4j
@RequiredArgsConstructor
public class TrafficInfoController {

    private final TransportService transportService;

 @GetMapping("/traffic-info/{transportMode}/top10")
    public List<TransportLineDto> top10(@PathVariable TransportMode transportMode) {
        return transportService.linesByTransportMode(transportMode);
    }

    @GetMapping("/traffic-info/{transportMode}/line/{lineNumber}")
    public TransportLineDto line(@PathVariable TransportMode transportMode, @PathVariable int lineNumber) {
        return transportService.lineInfo(transportMode, lineNumber);
    }

    @ExceptionHandler(NoSuchElementException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handle(NoSuchElementException e) {
        return "Could not find: " + e.getMessage();
    }

}
