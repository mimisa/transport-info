package com.code.transport.info.batch.processor;

import com.code.transport.info.entity.LineEntity;
import com.code.transport.info.model.TrafficLine;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class TrafficLineProcessor implements ItemProcessor<TrafficLine, LineEntity> {

    @Override
    public LineEntity process(TrafficLine trafficLine) throws Exception {
        return LineEntity.builder()
                .lineNumber(Integer.valueOf(trafficLine.getLineNumber()))
                .lineDesignation(trafficLine.getLineDesignation())
                .transportMode(trafficLine.getDefaultTransportMode())
                .transportModeCode(trafficLine.getDefaultTransportModeCode())
                .existsFromDate(trafficLine.getExistsFromDate())
                .lastModifiedUtcDateTime(trafficLine.getLastModifiedUtcDateTime())
                .build();
    }

}
