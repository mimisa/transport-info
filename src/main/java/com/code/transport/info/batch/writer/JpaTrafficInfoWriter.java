package com.code.transport.info.batch.writer;

import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Component
public class JpaTrafficInfoWriter<T> extends JpaItemWriter<T> {

    EntityManagerFactory entityManagerFactory;

    public JpaTrafficInfoWriter(EntityManagerFactory entityManagerFactory) {
        setEntityManagerFactory(entityManagerFactory);
    }

    @Override
    protected void doWrite(EntityManager entityManager, List<? extends T> items) {
        super.doWrite(entityManager, items);
    }

}
