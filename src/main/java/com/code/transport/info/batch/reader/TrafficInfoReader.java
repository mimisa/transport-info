package com.code.transport.info.batch.reader;

import com.code.transport.info.exception.TrafficInfoException;
import com.code.transport.info.model.TrafficInfoResponse;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Builder
public class TrafficInfoReader<T> implements ItemReader<T>, StepExecutionListener {
    private final ObjectMapper objectMapper;
    private final RestTemplate restTemplate;
    private final String apiUrl;
    private final String apiKey;
    private final String model;
    private int index=-0;
    private TrafficInfoResponse<T> response;
    private final Class<T> type;

    @Override
    public T read() {
        T item = null;
        if (containsData(response)) {
            item = response.getResponseData().get(index);
            index++;
        }
        return item;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
        fetchData();
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        if (containsData(response)) {
            return ExitStatus.EXECUTING;
        }
        return ExitStatus.COMPLETED;
    }

    private TrafficInfoResponse<T> fetchData() {
        String result = restTemplate.getForObject(getEndpoint(), String.class);
        JsonNode rootNode = null;
        try {
            rootNode = objectMapper.readTree(result);
            if(rootNode == null) throw new TrafficInfoException("Could not get paylod");

             String statusCode = rootNode.path("StatusCode").asText();
            if(statusCode.isBlank() || !statusCode.equals("0")) {
                throw new TrafficInfoException("Could not get payload. Statuscode:" + statusCode + " Reason: " +  rootNode.path("Message").asText());
            }
            ArrayNode data = (ArrayNode) rootNode.path("ResponseData").path("Result");

            List<T> dataElements = new ArrayList<>();
            Iterator<JsonNode> items = data.elements();
            items.forEachRemaining(node -> dataElements.add(objectMapper.convertValue(node, type)));
            response = TrafficInfoResponse.<T>builder().responseData(dataElements).build();
            return response;
        } catch (Exception e) {
            log.error("Not valid json: {}", e.getMessage(), e);
            throw new TrafficInfoException("Invalid payload", e);
        }
    }

    private boolean containsData(TrafficInfoResponse<T> response) {
        if (response == null) {
            return false;
        }
        if (response.getResponseData() == null || response.getResponseData().isEmpty()) {
            return false;
        }
        if(index >= response.getResponseData().size()) {
            return false;
        }
        return true;
    }

    private URI getEndpoint() {
        return UriComponentsBuilder
                .fromUriString(apiUrl)
                .queryParam("key", apiKey)
                .queryParam("model", model)
                .build().toUri();
    }
}
