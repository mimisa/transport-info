package com.code.transport.info.batch;

import com.code.transport.info.exception.TrafficInfoException;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Builder
@Slf4j
public class TrafficJobScheduler {

    private final List<Job> jobs;
    private final JobLauncher jobLauncher;
    private final CacheManager cacheManager;


    @Scheduled(cron = "${api.traffic.info.scheduling}")
    public void runJob() throws Exception {
        jobs.forEach(job -> {
            JobExecution execution = null;
            try {
                execution = jobLauncher.run(
                        job,
                        new JobParametersBuilder().addLong("JOB_EXECUTION_ " + job.getName(), System.nanoTime()).toJobParameters()
                );
            } catch (Exception e) {
                log.error("Failed to run job {} ", job.getName(), e);
                throw new TrafficInfoException("Failed to run job " + job.getName(), e);
            }
            log.info("Exit status: {}", execution.getStatus());
        });
        evictAllCache();
    }

    private void evictAllCache() {
        cacheManager.getCacheNames()
                .forEach(name -> cacheManager.getCache(name).clear());
    }
}
