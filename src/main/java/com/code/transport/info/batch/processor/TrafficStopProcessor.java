package com.code.transport.info.batch.processor;

import com.code.transport.info.entity.StopEntity;
import com.code.transport.info.model.TrafficStop;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class TrafficStopProcessor implements ItemProcessor<TrafficStop, StopEntity> {

    @Override
    public StopEntity process(TrafficStop stop) throws Exception {
        return StopEntity.builder()
                .stopPointNumber(Integer.valueOf(stop.getStopPointNumber()))
                .stopPointName(stop.getStopPointName())
                .stopAreaNumber(Integer.valueOf(stop.getStopAreaNumber()))
                .stopAreaTypeCode(stop.getStopAreaTypeCode())
                .zoneShortName(stop.getZoneShortName())
                .locationNorthingCoordinate(stop.getLocationNorthingCoordinate())
                .locationEastingCoordinate(stop.getLocationEastingCoordinate())
                .existsFromDate(stop.getExistsFromDate())
                .lastModifiedUtcDateTime(stop.getLastModifiedUtcDateTime())
                .build();
    }

}
