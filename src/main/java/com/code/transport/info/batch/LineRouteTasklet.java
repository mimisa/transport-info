package com.code.transport.info.batch;

import com.code.transport.info.service.TransportService;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LineRouteTasklet implements Tasklet {

    private final TransportService transportService;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        transportService.updateLinesWithRoutes();
        return RepeatStatus.FINISHED;
    }
}
