package com.code.transport.info.batch.processor;

import com.code.transport.info.entity.JourneyEntity;
import com.code.transport.info.model.TrafficJourney;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class TrafficJourneyProcessor implements ItemProcessor<TrafficJourney, JourneyEntity> {

    @Override
    public JourneyEntity process(TrafficJourney journey) throws Exception {
        return JourneyEntity.builder()
                .lineNumber(Integer.valueOf(journey.getLineNumber()))
                .directionCode(journey.getDirectionCode())
                .journeyPatternPointNumber(Integer.valueOf(journey.getJourneyPatternPointNumber()))
                .existsFromDate(journey.getExistsFromDate())
                .lastModifiedUtcDateTime(journey.getLastModifiedUtcDateTime())
                .build();
    }

}
