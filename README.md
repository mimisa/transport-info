# transport-info
This application is a REST service that uses [Trafficlab api](https://www.trafiklab.se/api/sl-hallplatser-och-linjer-2/dokumentation)
to fetch traffic info data for Stockholm region local traffic and provides 2 endpoints:

- The top 10 lines for a transport mode that has most stops on their itinerary
- Extended information about a line

## Prerequisits
- Java 11+
- Maven 3.2+
- Git

## Use guide
- Clone the repository to your local computer 
```
git clone https://bitbucket.org/mimisa/transport-info.git
```
- Build the project 
```
mvn clean install
```

- Run the application
```
mvn clean spring-boot:run
```

- Use it
   
```
 GET http://localhost:8080/traffic-info/[TRANSPORTMODE]/top10
 i.e.  http://localhost:8080/traffic-info/BUS/top10

 GET http://localhost:8080/traffic-info/[TRANSPORTMODE]/line/[LINENUMBER]
 i.e.  http://localhost:8080/traffic-info/BUS/line/56
```

## Approach
Assuming that the application will handle huge amount of data and many simultaneous users, I chose to store the info in a relational database.

* Batch jobs are used to fetch the data with Rest from the external API, write to the database, transform the data and store it in the database.
* A Scheduled task triggers the jobs daily at 3.00 am
* Results of search are cached. The cache is evicted after updates.

## Tech showcase
* Spring boot
* Spring Batch
* Spring data jpa, Hibernate, in-memory database H2
* Spring Rest
* Spring cron scheduler
* Caffeine cache
* Modelmapper
* Lombok


